package se.erikostlund.dumpsmtpd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.stream.Collectors;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.subethamail.smtp.MessageContext;
import org.subethamail.smtp.MessageHandler;
import org.subethamail.smtp.MessageHandlerFactory;
import org.subethamail.smtp.RejectException;
import org.subethamail.smtp.TooMuchDataException;
import org.subethamail.smtp.server.SMTPServer;

/**
 *
 * @author Erik
 */
public class Main {

    public static void main(String[] args) throws IOException, InterruptedException {

        Map<String, String> environment = System.getenv();

        SMTPServer.port(Integer.parseInt(environment.getOrDefault("DUMPSMTPD_PORT", "8025")))
                .bindAddress(InetAddress.getByName(environment.getOrDefault("DUMPSMTPD_BIND_ADDRESS", "localhost")))
                .hostName(environment.getOrDefault("HOSTNAME", environment.getOrDefault("COMPUTERNAME", "UNKNOWN_HOST")))
                .messageHandlerFactory(new MessageHandlerFactoryImpl())
                .build()
                .start();
    }
}

class MessageHandlerFactoryImpl implements MessageHandlerFactory {

    @Override
    public MessageHandler create(MessageContext mc) {
        return new MessageHandlerImpl();
    }

}

class MessageHandlerImpl implements MessageHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageHandlerImpl.class);

    @Override
    public void from(String from) throws RejectException {
        // Do nothing
    }

    @Override
    public void recipient(String recipient) throws RejectException {
        // Do nothing
    }

    @Override
    public void data(InputStream data) throws RejectException, TooMuchDataException, IOException {

        try {
            MimeMessage message = new MimeMessage(null, data);

            System.out.println(new StringBuilderWrapper()
                    .append(System.lineSeparator())
                    .appendIfPresent("Date: ", message.getSentDate())
                    .appendIfPresent("To: ", message.getRecipients(Message.RecipientType.TO))
                    .appendIfPresent("Cc: ", message.getRecipients(Message.RecipientType.CC))
                    .appendIfPresent("Bcc: ", message.getRecipients(Message.RecipientType.BCC))
                    .appendIfPresent("From: ", message.getFrom())
                    .appendIfPresent("Subject: ", message.getSubject())
                    .append(message.getInputStream())
                    .toString());

        } catch (MessagingException ex) {
            LOGGER.error("There was an error processing the message.", ex);
        }
    }

    @Override
    public void done() {
        // Do nothing
    }

}

class StringBuilderWrapper {

    private final DateFormat dateFormat;
    private final StringBuilder stringBuilder;

    public StringBuilderWrapper() {
        dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ [z]");
        stringBuilder = new StringBuilder();
    }

    public StringBuilderWrapper appendIfPresent(String s1, String s2) {
        if (s2 != null && !s2.isEmpty()) {
            stringBuilder.append(s1);
            stringBuilder.append(s2);
            stringBuilder.append(System.lineSeparator());
        }

        return this;
    }

    public StringBuilderWrapper appendIfPresent(String s, Date d) {
        if (d != null) {
            stringBuilder.append(s);
            stringBuilder.append(dateFormat.format(d));
            stringBuilder.append(System.lineSeparator());
        }

        return this;
    }

    public StringBuilderWrapper appendIfPresent(String s, Object[] arr) {
        if (arr != null) {
            stringBuilder.append(s);
            stringBuilder.append(Arrays.toString(arr));
            stringBuilder.append(System.lineSeparator());
        }

        return this;
    }

    public StringBuilderWrapper append(String s) {
        stringBuilder.append(s);

        return this;
    }

    public StringBuilderWrapper append(InputStream is) throws IOException {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
            stringBuilder.append(System.lineSeparator());
            stringBuilder.append(br.lines().collect(Collectors.joining(System.lineSeparator())));
        }

        return this;
    }

    @Override
    public String toString() {
        return stringBuilder.toString();
    }
}
