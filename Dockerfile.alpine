FROM maven:3-jdk-8-alpine as maven
COPY pom.xml /usr/src/app/pom.xml
RUN mvn -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn --batch-mode --file /usr/src/app/pom.xml --quiet dependency:go-offline
COPY src /usr/src/app/src
RUN mvn --batch-mode --file /usr/src/app/pom.xml clean package

FROM ubuntu:18.04 as graal
RUN apt-get update && apt-get install -y gcc zlib1g-dev
RUN apt-get update && apt-get -yq install build-essential wget zlib1g-dev && wget -qO- https://github.com/oracle/graal/releases/download/vm-1.0.0-rc6/graalvm-ce-1.0.0-rc6-linux-amd64.tar.gz | tar xz -C /opt
COPY --from=maven /usr/src/app/target/lib /opt/app/lib
COPY --from=maven /usr/src/app/target/*.jar /opt/app
RUN cd /opt/app && /opt/graalvm-ce-1.0.0-rc6/bin/native-image --no-server --static -jar ./dumpsmtpd-0.1.jar

FROM alpine:3.8
COPY --from=graal /opt/app/dumpsmtpd-0.1 /usr/local/bin/dumpsmtpd
EXPOSE 8025/tcp
ENTRYPOINT ["/usr/local/bin/dumpsmtpd"]